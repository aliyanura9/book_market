from django.urls import path
from users.views import RegistrationViewSet,\
                        LoginViewSet


urlpatterns = [
    path('registration/', RegistrationViewSet.as_view({"post": 'create'}), name='reqistration'),
    path('login/', LoginViewSet.as_view({"post": 'post'}), name='login'),
]
