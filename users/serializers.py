from rest_framework import serializers
from django.contrib.auth.models import User


class RegistrationSerializer(serializers.ModelSerializer):
    '''User login serializer
    required fields: username, password
    fields: avatar, username, nickname, password'''
    password2 = serializers.CharField(max_length=20, required=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'password2')


class LoginSerializer(serializers.Serializer):
    '''User login serializer
    required fields: username, password'''

    username = serializers.CharField(min_length=2, required=True)
    password = serializers.CharField(min_length=2, required=True)
