FROM python:3.10

ENV PYTHONUNBUFFERED 1

RUN mkdir -p /book_market
WORKDIR /book_market

COPY requirements.txt /book_market/
COPY . /book_market/

RUN pip install -r requirements.txt
