from django.contrib import admin
from sales.models import Sale


@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    list_display = ('book', 'buyer', 'created_at')
    list_display_links = ('book',)
    list_filter = ('book', 'buyer', 'created_at')
