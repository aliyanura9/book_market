from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAdminUser
from rest_framework.authentication import TokenAuthentication
from sales.serializers import SaleSerializer
from sales.services import SaleService



class SaleViewSet(ModelViewSet):
    permission_classes = (IsAdminUser,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = SaleSerializer

    def list(self, request, *args, **kwargs):
        queryset, sales_sum = SaleService.filter(
            created_at_start=request.GET.get('created_at_start', None),
            created_at_end=request.GET.get('created_at_end', None),
        )
        data = self.serializer_class(queryset, many=True).data
        return Response(data={
            'result': data,
            'sales_sum': sales_sum,
            'status': 'OK'
        }, status=status.HTTP_200_OK)
    
    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        SaleService.create(
            book=serializer.validated_data.get('book'),
            buyer=serializer.validated_data.get('buyer'),
        )
        return Response(data={
            'message': 'The sale has been successfully created',
            'status': 'CREATED'
        }, status=status.HTTP_201_CREATED)
