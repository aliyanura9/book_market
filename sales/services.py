from datetime import date
from django.db.models import Sum
from sales.models import Sale
from users.exceptions import ObjectNotFoundException


class SaleService:
    model = Sale

    @classmethod
    def get(cls, *args, **kwargs) -> model:
        try:
            return cls.model.objects.get(*args, **kwargs)
        except cls.model.DoesNotExist:
            raise ObjectNotFoundException(f'{cls.model.__name__} not found')

    @classmethod
    def filter(cls, created_at_start, created_at_end):
        sales = cls.model.objects.filter()
        if created_at_start:
            created_at_start = created_at_start.split('-')
            sales = sales.filter(created_at__date__gte=date(
                int(created_at_start[0]),
                int(created_at_start[1]),
                int(created_at_start[2]),
            ))
        if created_at_end:
            created_at_end = created_at_end.split('-')
            sales = sales.filter(created_at__date__lte=date(
                int(created_at_end[0]),
                int(created_at_end[1]),
                int(created_at_end[2]),
            ))
        sales_sum = sales.aggregate(Sum('book__price')).get('book__price__sum')
        return sales, sales_sum
    
    @classmethod
    def create(cls, book, buyer):
        cls.model.objects.create(
            book=book,
            buyer=buyer
        )
