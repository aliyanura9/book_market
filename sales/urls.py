from django.urls import path
from sales.views import SaleViewSet


urlpatterns = [
    path('sales/', SaleViewSet.as_view({'get': 'list', 'post':'create'})),
]
