from django.db import models
from django.contrib.auth.models import User
from books.models import Book


class Sale(models.Model):
    buyer = models.ForeignKey(User, on_delete=models.CASCADE,
                              related_name='buies',
                              verbose_name='Покупатель')
    book = models.ForeignKey(Book, models.CASCADE,
                             related_name='sales',
                             verbose_name='книга')
    created_at = models.DateTimeField(auto_now=True,
                                      verbose_name='Дата реализации')
    
    def __str__(self) -> str:
        return f'{self.book} bought by {self.buyer} at {self.created_at}'
    
    class Meta:
        db_table = 'sales'
        verbose_name = 'Продажа'
        verbose_name_plural = 'Продажи'
        ordering = ('-created_at',)