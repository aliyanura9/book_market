from rest_framework import serializers
from sales.models import Sale


class SaleSerializer(serializers.ModelSerializer):
    buyer_name = serializers.CharField(source='buyer.username', required=False)
    book_name = serializers.CharField(source='book.name', required=False)
    class Meta:
        model = Sale
        fields = ('buyer_name', 'buyer', 'book_name',
                  'book', 'created_at')
        extra_kwargs = {
            'created_at': {'required': False},
        }