from django.db import models


class Book(models.Model):
    name = models.CharField(max_length=250, verbose_name='Название')
    author = models.CharField(max_length=250, verbose_name='Автор')
    description = models.TextField(verbose_name='Описание')
    price = models.FloatField(verbose_name='Цена')

    def __str__(self) -> str:
        return self.name
    
    class Meta:
        db_table = 'books'
        verbose_name = 'Книга'
        verbose_name_plural = 'Книги'
        ordering = ('name',)
