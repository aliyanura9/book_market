from django.contrib import admin
from books.models import Book


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ('name', 'author', 'price')
    list_display_links = ('name',)
    list_filter = ('name', 'author', 'price')
