from books.models import Book
from users.exceptions import ObjectNotFoundException


class BookService:
    model = Book

    @classmethod
    def get(cls, *args, **kwargs) -> model:
        try:
            return cls.model.objects.get(*args, **kwargs)
        except cls.model.DoesNotExist:
            raise ObjectNotFoundException(f'{cls.model.__name__} not found')


    @classmethod
    def filter(cls, *args, **kwargs):
        return cls.model.objects.filter(**kwargs)
    
    @classmethod
    def create(cls, name: str, description: str,
               author: str, price: float):
        cls.model.objects.create(
            name=name,
            description=description,
            author=author,
            price=price
        )

    @classmethod
    def update(cls, id:int, name: str, description: str,
               author: str, price: float):
        book = cls.get(id=id)
        book.name = name
        book.description = description
        book.author = author
        book.price = price
        book.save()
    
    @classmethod
    def delete(cls, id:int):
        book = cls.get(id=id)
        book.delete()
