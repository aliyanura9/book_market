from rest_framework import status
from rest_framework.filters import SearchFilter
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAdminUser
from rest_framework.authentication import TokenAuthentication
from rest_framework.viewsets import ModelViewSet
from books.serializers import BookSerializer
from books.services import BookService


class BookViewSet(ModelViewSet):
    permission_classes = (AllowAny,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = BookSerializer
    lookup_field = 'pk'
    queryset = BookService.filter()

    def get_permissions(self):
        if self.request.method == 'GET':
            return [permission() for permission in (AllowAny,)]
        else:
            return [permission() for permission in (IsAdminUser,)]
    
    def retrieve(self, request, *args, **kwargs):
        book = BookService.get(id=kwargs.get('pk'))
        data = self.serializer_class(book, many=False).data
        return Response(data={
            'result': data,
            'status': 'OK'
        }, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        BookService.create(
            name=serializer.validated_data.get('name'),
            author=serializer.validated_data.get('author'),
            description=serializer.validated_data.get('description'),
            price=serializer.validated_data.get('price')
        )
        return Response(data={
            'message': 'The book has been successfully created',
            'status': 'CREATED'
        }, status=status.HTTP_201_CREATED)
    
    def update(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        BookService.update(
            id=kwargs.get('pk'),
            name=serializer.validated_data.get('name'),
            author=serializer.validated_data.get('author'),
            description=serializer.validated_data.get('description'),
            price=serializer.validated_data.get('price')
        )
        return Response(data={
            'message': 'The book has been successfully updated',
            'status': 'OK'
        }, status=status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        BookService.delete(
            id=kwargs.get('pk'),
        )
        return Response(data={
            'message': 'The book has been successfully deleted',
            'status': 'NO CONTENT'
        }, status=status.HTTP_204_NO_CONTENT)
